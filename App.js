import React, { Component } from 'react';
import { createSwitchNavigator, createBottomTabNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements';

import LoginScreen from './src/screens/Login';
import DashboardScreen from './src/screens/Dashboard';
import { APP_COLOR } from './src/utils/constants';
import OtherTabsScreen from './src/screens/OtherTabs';
import InnovareWebsiteScreen from './src/screens/InnovareWebsite';

const DashboardBottomTab = createBottomTabNavigator({
  Screen1: DashboardScreen,
  Screen2: InnovareWebsiteScreen,
  Screen3: OtherTabsScreen,
  Screen4: OtherTabsScreen,
  Screen5: OtherTabsScreen,
},
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Screen1') {
          iconName = 'home';
        } else if (routeName === 'Screen2') {
          iconName = 'globe';
        } else if (routeName === 'Screen3') {
          iconName = '500px';
        } else if (routeName === 'Screen4') {
          iconName = 'bell';
        } else if (routeName === 'Screen5') {
          iconName = 'user';
        }
        return <Icon name={iconName} type={'entypo'} size={24} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'white',
      inactiveTintColor: '#DCD5CF',
      showLabel: false,
      tabStyle: {
        backgroundColor: APP_COLOR
      }
    },
  }
)



const AppNavigator = createSwitchNavigator({
  Auth: LoginScreen,
  Dashboard: DashboardBottomTab,
});

export default class App extends Component {
  render() {
    return (<AppNavigator />);
  }
}