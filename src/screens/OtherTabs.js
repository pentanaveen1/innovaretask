import React from 'react';
import { View, Text, Button } from 'react-native';

export default class OtherTabsScreen extends React.Component {

    logout = () => this.props.navigation.navigate('Auth');

    render() {
        return (<View>
            <Text style={{ textAlign: "center", fontSize: 32 }}>
            {this.props.navigation.state.routeName}
            </Text>
            <Button style={{padding: 40}} title="Logout" onPress={this.logout}/>
            </View>)
    }
}