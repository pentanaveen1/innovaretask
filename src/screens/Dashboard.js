import React from 'react';
import { View, FlatList, Text, TextInput } from 'react-native';
import { Icon, Avatar } from 'react-native-elements';

import { APP_COLOR } from '../utils/constants';
import InnovareDashboardCard from '../components/InnovareDashboardCard';
import InnovareHeader from '../components/InnovareHeader';
import InnovareStatusBar from '../components/InnovareStatusBar';
import InnovareIconWithText from '../components/InnovareIconWithText';

export default class DashboardScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            feed: []
        };
    }

    componentDidMount() {
        this.getFeed();
    }

    async getFeed() {
        try {
            const feedString = await fetch('http://www.json-generator.com/api/json/get/ceWnfnWAbS?indent=2');
            const feed = await feedString.json();
            console.log(feed);
            this.setState({ feed });
        } catch (error) {
            console.error(error);
        }

    }

    _keyExtractor = (item) => item.id.toString();

    /*
        item: {
            feed_image
            feed_date
            feed_description 
            feed_pic
            feed_name
            id
        }
    */
    _renderItem = ({ item }) => <InnovareDashboardCard item={item}/>

    renderItemSeparator = () => (<View
        style={{
            height: 8,
            backgroundColor: "lightgray"
        }}
    />);

    flatListHeaderComponent = () => {
        return (
            <View style={{ flexDirection: 'row', padding: 16, backgroundColor: 'white', borderBottomColor: 'lightgray', borderBottomWidth: 8 }}>
                <View style={{ display: "flex", flex: 1, alignItems: 'center', marginTop: 20 }}>
                    <Avatar
                        rounded
                        medium
                        icon={{ name: 'person' }}
                        containerStyle={{ backgroundColor: APP_COLOR }}
                        style={{ flexDirection: 'row', alignItems: "center" }}
                    />
                </View>
                <View style={{ display: "flex", flex: 3 }}>
                    <TextInput style={{ borderColor: "gray", borderWidth: 1, borderRadius: 16, }}
                    placeholder={`What's on your mind? Write here...`}
                    multiline={true}
                        numberOfLines={3} />
                    <View style={{ flexDirection: 'row', alignItems: "center", justifyContent: "space-between", alignSelf: "auto" }}>
                        <InnovareIconWithText name="live-tv" text="Live"/>
                        <InnovareIconWithText name="md-photos" type="ionicon" text="Photos"/>
                        <InnovareIconWithText name="flag" text="Events"/>
                    </View>
                </View>
            </View>)
    }

    render() {
        return (<View> 
            <InnovareStatusBar />
            <InnovareHeader />
            <FlatList
                data={this.state.feed}
                ListHeaderComponent={this.flatListHeaderComponent}
                renderItem={this._renderItem}
                keyExtractor={this._keyExtractor}
                ItemSeparatorComponent={this.renderItemSeparator}
                style={{
                    marginBottom: 48
                }}
            />
        </View>)
    }
}