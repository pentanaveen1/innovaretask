import React from 'react';
import { View, TextInput, Text, Dimensions, StyleSheet, CheckBox, StatusBar } from 'react-native';
import { Button, Icon } from 'react-native-elements'

import InnovareInput from '../components/InnovareInput';
import InnovareStatusBar from '../components/InnovareStatusBar';
import { APP_COLOR } from '../utils/constants';

const { height } = Dimensions.get('window');

export default class LoginScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            rememberMe: false,
            socialIcons: [
                { iconName: 'sc-facebook' },
                { iconName: 'sc-twitter' },
                { iconName: 'sc-google-plus' }
            ]
        }
    }

    onContinue = async () => {
        //userId: innovare
        //password: innovare@7
        const { username, password } = this.state;
        try {
            const loginResponseString = await fetch('http://35.154.185.95/auth/local/login', {
                method: 'POST',
                body: JSON.stringify({
                    userId: username,
                    password: password,
                }),
            });
            const loginResponse = await loginResponseString.json();
            console.log(loginResponse);
        } catch (error) {
            console.error(error);
        } finally{
            if(username == 'innovare' && password == 'innovare@7'){
                this.props.navigation.navigate('Dashboard');
            } else{
                alert("Invalid credentials");
            }
        }

    }

    onRememberMeToggle = rememberMe => this.setState({ rememberMe });

    onUserName = username => this.setState({ username });

    onPassword = password => this.setState({ password });

    render() {
        return (<View style={styles.loginContainer}>
            <InnovareStatusBar />

            <InnovareInput
                iconName="user"
                placeholder="Enter username or email"
                onChangeText={this.onUserName}
                value={this.state.username}
                secureTextEntry={false} />

            <InnovareInput
                iconName="lock"
                placeholder="Enter password"
                onChangeText={this.onPassword}
                value={this.state.password}
                secureTextEntry={true} />

            <View style={styles.flexDirectionRow}>
                <CheckBox onValueChange={this.onRememberMeToggle} value={this.state.rememberMe} ></CheckBox>
                <Text style={{ color: APP_COLOR }}>Remember me?</Text>
            </View>

            <Button
                title='Continue'
                rounded={true}
                backgroundColor={APP_COLOR}
                onPress={this.onContinue}
            />
            <Text style={styles.textAlignCenter}>Forgot password?</Text>

            <Text style={styles.textAlignCenter}>────────  Or Login with  ────────</Text>

            <View style={[styles.flexDirectionRow, styles.justifyContentCenter]}>
                {this.state.socialIcons.map(
                    (icon, index) => (
                        <Icon
                            key={index.toString()}
                            reverse
                            color='white'
                            containerStyle={styles.iconStyle}
                            type='evilicon'
                            name={icon.iconName}
                            size={24}
                            iconStyle={{ color: APP_COLOR }}
                        />
                    )
                )}
            </View>

            <Text style={styles.textAlignCenter}>Don't have an account</Text>

            <Button title='Register now' rounded={true} transparent={true} color={APP_COLOR}
                buttonStyle={styles.registerNow}
            />
        </View>)
    }
}

const styles = StyleSheet.create({
    loginContainer: {
        backgroundColor: 'white',
        padding: 40,
        height: height,
        justifyContent: "center"
    },
    registerNow: {
        borderColor: APP_COLOR,
        borderWidth: 2 
    },
    iconStyle: {
        borderColor: APP_COLOR,
        borderWidth: StyleSheet.hairlineWidth
    },
    textAlignCenter: {
        textAlign: 'center',
        color: APP_COLOR,
        paddingVertical: 16
    },
    flexDirectionRow: {
        display: "flex",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: 'center'
    },
    justifyContentCenter: {
        justifyContent: "center",
    }
});