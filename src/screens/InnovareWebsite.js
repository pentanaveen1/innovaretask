import React from 'react';
import { View, WebView, ActivityIndicator } from 'react-native';

export default class InnovareWebsiteScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true
        }
    }

    stopLoader = () => this.setState({ isLoading: false });

    render() {
        return (
            <View style={{flex: 1}}>
                {this.state.isLoading ? (<ActivityIndicator size="large" />) : null}
                <WebView onLoadEnd={this.stopLoader}
                    source={{ uri: 'http://itsinnovare.com/' }}
                />
            </View>
        )
    }
}