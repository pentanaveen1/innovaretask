import React from 'react';
import { View, TextInput, Dimensions, StyleSheet } from 'react-native';
import { Header, Icon } from 'react-native-elements';

import { APP_COLOR } from '../utils/constants';

const { width } = Dimensions.get('window');

export default class InnovareHeader extends React.Component {

    renderHeaderSearchBar() {
        return (
            <View style={styles.searchBar}><TextInput editable={false}/></View>
        );
    }

    renderHeaderRightComponent() {
        return (
            <View style={styles.rightComponentContainer}>
                <Icon name="person-add" color="white" iconStyle={styles.headerIcon} />
                <Icon name="comment-text-multiple-outline" 
                type="material-community" color="white" iconStyle={styles.headerIcon}  />
                <Icon name="menu" color="white" />
            </View>
        )
    }

    render() {
        return (<Header backgroundColor={APP_COLOR} 
            outerContainerStyles={{ height: 48 }} innerContainerStyles={{ alignContent: "center" }}
            leftComponent={{ icon: 'camera', color: '#fff', type: "entypo" }}
            centerComponent={this.renderHeaderSearchBar()}
            rightComponent={this.renderHeaderRightComponent()}
        />)
    }
}

const styles = StyleSheet.create({
    rightComponentContainer:{ 
        flexDirection: 'row',
        alignItems: "center"
    },
    searchBar: {
        borderRadius: 20,
        color: 'white',
        backgroundColor: 'white',
        paddingLeft: 8,
        width: width/2 
    },
    headerIcon: {
        marginRight: 12
    }
})