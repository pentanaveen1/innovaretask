import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { Icon, Avatar } from 'react-native-elements';

import InnovareIconWithText from './InnovareIconWithText';
import { APP_COLOR } from '../utils/constants';

export default class InnovareDashboardCard extends React.Component {
    render() {
        const { item } = this.props;
        return (
            <View style={styles.cardContainer}>

                <View style={styles.cardHeaderAndFooterContainer}>
                    <View style={{ flexDirection: 'row', alignItems: "center" }}>
                        <Avatar
                            rounded
                            medium
                            icon={{ name: 'person' }}
                            containerStyle={styles.avatarContainer}
                        />
                        <View>
                            <Text style={styles.cardHeading}>{item.feed_name}</Text>
                            <Text>{item.feed_date}</Text>
                        </View>

                    </View>
                    <View><Icon type="entypo"
                        name="dots-three-horizontal" color={APP_COLOR} size={20} /></View>
                </View>

                <View style={styles.contentContainer}>
                    <Text>{item.feed_description}</Text>
                    <Image source={{ uri: item.feed_pic }} style={styles.imageContainer} />
                </View>

                <View style={styles.cardHeaderAndFooterContainer}>
                    <View style={{ flexDirection: 'row', alignItems: "center" }}>
                        <InnovareIconWithText type="entypo" name="heart" text="160" />
                        <InnovareIconWithText name="mode-comment" text="200" />
                    </View>
                    <InnovareIconWithText type="font-awesome" name="share" text="120" />
                </View>

            </View>
        );
    }
}


const styles = StyleSheet.create({
    cardContainer: {
        padding: 24,
        backgroundColor: 'white'
    },
    cardHeaderAndFooterContainer: {
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: "center"
    },
    avatarContainer:{
        backgroundColor: APP_COLOR,
        marginRight: 12
    },
    cardHeading:{
        fontSize: 18,
        fontWeight: "bold",
        color: APP_COLOR
    },
    contentContainer: {
        marginVertical: 20
    },
    imageContainer: {
        height: 200,
        marginTop: 16
    }
});