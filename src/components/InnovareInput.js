import React from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements'

import { APP_COLOR } from '../utils/constants';

export default class InnovareInput extends React.Component {

    render() {
        return (<View
            style={styles.inputContainer}>
            <Icon name={this.props.iconName} size={20} color={APP_COLOR} type="entypo"/>
            <TextInput
                style={styles.loginInput}
                placeholderTextColor={APP_COLOR}
                selectionColor={APP_COLOR}
                placeholder={this.props.placeholder}
                onChangeText={this.props.onChangeText}
                value={this.props.value}
                secureTextEntry={this.props.secureTextEntry}
            />
        </View>);
    }
}

const styles = StyleSheet.create({
    inputContainer: {
        display: "flex",
        flexDirection: "row",
        paddingVertical: 10,
        alignItems: 'center',
        height: 40,
        borderBottomColor: APP_COLOR,
        borderBottomWidth: StyleSheet.hairlineWidth,
        marginVertical: 8

    },
    loginInput: {
        height: 40,
        fontSize: 16,
        color: 'black',
    },
})