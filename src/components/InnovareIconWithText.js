import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';

import { APP_COLOR } from '../utils/constants';

export default class InnovareIconWithText extends React.Component {
    render() {
        return (
        <View style={styles.container}>
            <Icon iconStyle={styles.iconMargin}
             name={this.props.name} type={this.props.type} color={APP_COLOR} />
            <Text style={styles.iconText}>{this.props.text}</Text>
        </View>);
    }
}

const styles = StyleSheet.create({
    container: { 
        flexDirection: 'row',
        alignItems: 'center',
        margin: 4,
        marginRight: 16
     },
     iconMargin: {
         marginRight: 4
     },
     iconText: {
         color: APP_COLOR
     }
})