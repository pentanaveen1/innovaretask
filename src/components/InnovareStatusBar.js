import React from 'react';
import { StatusBar } from 'react-native';

import { APP_COLOR } from '../utils/constants';

export default class InnovareStatusBar extends React.Component {
    render() {
        return (<StatusBar
            backgroundColor={APP_COLOR}
            barStyle="light-content"
        />)
    }
}